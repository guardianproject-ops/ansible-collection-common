<!-- markdownlint-disable -->
## Roles

| Role Name    | Description  |
| ------------ | ------------ |
| [gpops.common.cloudflared](./roles/cloudflared) | This ansible role installs cloudflared and configures for use with argo tunnel. |
| [gpops.common.debian_baseline](./roles/debian_baseline) | Baseline configuration for a debian box |
| [gpops.common.docker_host](./roles/docker_host) | Installs docker daemon and tools |
| [gpops.common.node_exporter](./roles/node_exporter) | Installs node-exporter |
| [gpops.common.ntp](./roles/ntp) | Installs and configures chrony |
| [gpops.common.podman_matrix](./roles/podman_matrix) | Keanu matrix stack |
| [gpops.common.rds_provisioner_instance_create](./roles/rds_provisioner_instance_create) | Create RDS provisioner instance |
| [gpops.common.rds_provisioner_instance_destroy](./roles/rds_provisioner_instance_destroy) | Create RDS provisioner instance |
| [gpops.common.unattended_upgrades](./roles/unattended_upgrades) | Installs and configures unattended_upgrades on Debian hosts |
| [gpops.common.vector](./roles/vector) | Install and configure vector for shipping logs and metrics around |

## Playbooks

| Playbook Name    | Description  |
| ---------------- | ------------ |
| [gpops.common.ami-debian-base](./playbooks/gpops.common.ami-debian-base.yml) | *TODO* |
| [gpops.common.instance_base](./playbooks/gpops.common.instance_base.yml) | *TODO* |
| [gpops.common.many_mirrors_infra](./playbooks/gpops.common.many_mirrors_infra.yml) | Deploy infrastructure resources with Terraform to AWS |
| [gpops.common.matrix_infra](./playbooks/gpops.common.matrix_infra.yml) | Deploy infrastructure resources with Terraform to AWS |
| [gpops.common.matrix_instance](./playbooks/gpops.common.matrix_instance.yml) | Ensure instance base is configured |
| [gpops.common.monitoring_infra](./playbooks/gpops.common.monitoring_infra.yml) | Deploy infrastructure resources with Terraform to AWS |
| [gpops.common.monitoring_instance](./playbooks/gpops.common.monitoring_instance.yml) | Ensure instance base is configured |
| [gpops.common.operations_infra](./playbooks/gpops.common.operations_infra.yml) | Deploy infrastructure resources with Terraform to AWS |
| [gpops.common.prometheus](./playbooks/gpops.common.prometheus.yml) | *TODO* |
| [gpops.common.rds](./playbooks/gpops.common.rds.yml) | *TODO* |

<!-- markdownlint-enable -->
