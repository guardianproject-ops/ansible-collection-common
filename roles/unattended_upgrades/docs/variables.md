## Role Variables

* `unattended_upgrades_enabled`: `true` - When disabled, the package is not uninstalled, rather the configuration is changed to disable updates and upgrades.



* `unattended_upgrades_periodic_autoclean`: `7` - Clean up obsolete and removed packages in APT cache every n days.



* `unattended_upgrades_periodic_verbosity`: `0` - Specify verbosity level of the :file:`/etc/cron.daily/apt` script, supported levels are 0-3, higher level means higher verbosity. Enabling this option will result in mails from :program:`cron` with the script output being sent to ``root``.



* `unattended_upgrades_auto_reboot`: `false` - Automatically reboot the host without confirmation after unattended upgrade if any packages require it.



* `unattended_upgrades_mail_from`: `''` - The email address written to FROM field. If empty, use the /usr/bin/unattended-upgrade default



* `unattended_upgrades_mail_to`: `[]` - List of e-mail addresses to which the :program:`unattended-upgrade` script will sent the e-mails with upgrade results. Specify empty list to disable e-mails.



* `unattended_upgrades_mail_only_on_error`: `true` - Enable or disable an option to only send mail messages when errors occur during unattended upgrades.



* `unattended_upgrades_listchanges_enabled`: `false` - Enable or disable apt-listchanges


