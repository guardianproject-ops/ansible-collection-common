## Requirements

None.

## Example Playbook

```yaml
- hosts: servers
  vars:
    timezone: Europe/Germany
  roles:
    - ansible-ntp
```
