#!/usr/bin/env python3
import shutil
import pathlib
import ipaddress
import json
import sys
import os
import structlog
from prometheus_client import CollectorRegistry, Gauge, write_to_textfile
import subprocess
from typing import List

import requests

structlog.configure(processors=[structlog.processors.JSONRenderer()])
log = structlog.get_logger()
NGINX_PROXY_CONF_PATH = os.environ.get("NGINX_PROXY_CONF_PATH", None)
NGINX_PROXY_METRICS_PATH = os.environ.get("NGINX_PROXY_METRICS_PATH", None)
NGINX_PROXY_WORKDIR = os.environ.get("NGINX_PROXY_WORKDIR", None)


def is_path_writable(path) -> bool:
    if os.path.exists(path):
        return os.access(path, os.W_OK)
    else:
        dir_path = os.path.dirname(path)
        return os.access(dir_path, os.W_OK) if dir_path else False


if NGINX_PROXY_CONF_PATH is None:
    sys.stderr.write(f"The env var NGINX_PROXY_CONF_PATH is not set!")
    sys.exit(1)

if not is_path_writable(NGINX_PROXY_CONF_PATH):
    sys.stderr.write(
        f"The path NGINX_PROXY_CONF_PATH={NGINX_PROXY_CONF_PATH} is not writable"
    )
    sys.exit(1)

if NGINX_PROXY_METRICS_PATH is None:
    sys.stderr.write(f"The env var NGINX_PROXY_METRICS_PATH is not set!")
    sys.exit(1)

if not is_path_writable(NGINX_PROXY_METRICS_PATH):
    sys.stderr.write(
        f"The path NGINX_PROXY_METRICS_PATH={NGINX_PROXY_METRICS_PATH} is not writable"
    )
    sys.exit(1)

if NGINX_PROXY_WORKDIR is None or NGINX_PROXY_WORKDIR == "":
    sys.stderr.write(f"The env var NGINX_PROXY_WORKDIR is not set!")
    sys.exit(1)

if not is_path_writable(NGINX_PROXY_WORKDIR + "/test"):
    sys.stderr.write(
        f"The path NGINX_PROXY_WORKDIR={NGINX_PROXY_WORKDIR} is not writable"
    )
    sys.exit(1)


def validate_cidrs(cidr_list: List[str]) -> List[str]:
    if cidr_list is None:
        return []
    valid_cidrs = []
    for cidr in cidr_list:
        try:
            ipaddress.ip_network(cidr.strip())
            valid_cidrs.append(cidr)
        except ValueError:
            pass
    return valid_cidrs


def get_cloudflare_ip_ranges() -> List[str]:
    url = "https://www.cloudflare.com/ips-v4/"
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.text.strip().split("\n")
    except requests.RequestException as e:
        log.error("failed to fetch cloudflare ips", url=url, exc_info=e)
        return []


def get_cloudfront_ip_ranges() -> List[str]:
    cloudfront_ip_ranges_url = (
        "https://d7uri8nf7uskq.cloudfront.net/tools/list-cloudfront-ips"
    )
    cloudfront_backup_ip_ranges_url = "https://ip-ranges.amazonaws.com/ip-ranges.json"
    ip_ranges = []

    try:
        page_content = requests.get(cloudfront_ip_ranges_url, timeout=10)
        ip_ranges_json = json.loads(page_content.text)
        ip_ranges = sorted({x for v in ip_ranges_json.values() for x in v})

    except requests.exceptions.RequestException as e:
        log.warn(
            "failed to fetch cloudfront ips from primary source",
            url=cloudfront_ip_ranges_url,
            exc_info=e,
        )
        try:
            page_content = requests.get(cloudfront_backup_ip_ranges_url, timeout=10)
            ip_ranges_json = json.loads(page_content.text)
            filtered_v4_ip_ranges = [
                x["ip_prefix"]
                for x in ip_ranges_json["prefixes"]
                if x["service"] == "CLOUDFRONT"
            ]
            filtered_v6_ip_ranges = [
                x["ipv6_prefix"]
                for x in ip_ranges_json["ipv6_prefixes"]
                if x["service"] == "CLOUDFRONT"
            ]
            ip_ranges = filtered_v4_ip_ranges + filtered_v6_ip_ranges
        except requests.exceptions.RequestException:
            log.error(
                "failed to fetch cloudfront ips from backup source",
                url=cloudfront_backup_ip_ranges_url,
                exc_info=e,
            )
            return []

    return ip_ranges


def write_prom_metrics(registry):
    write_to_textfile(NGINX_PROXY_METRICS_PATH, registry)


def format_nginx_conf(ip_list: List[str]) -> str:
    conf = ""
    for ip in ip_list:
        conf += f"set_real_ip_from {ip};\n"
    return conf


def write_nginx_conf(path: str, conf: str) -> bool:
    try:
        with open(path, "w") as f:
            f.write(conf)
        return True
    except IOError as e:
        log.error(
            "failed to write nginx config",
            nginx_conf_path=path,
            exc_info=e,
        )
        return False


def backup_file(original_path) -> bool:
    backup_path = pathlib.Path(
        NGINX_PROXY_WORKDIR, os.path.basename(original_path) + ".bak"
    )
    if not os.path.exists(original_path):
        return True
    try:
        shutil.copyfile(original_path, backup_path)
        return True
    except IOError as e:
        log.error(
            "failed to backup file",
            original_path=original_path,
            backup_path=backup_path,
            exc_info=e,
        )
        return False


def restore_file(original_path) -> bool:
    backup_path = pathlib.Path(
        NGINX_PROXY_WORKDIR, os.path.basename(original_path) + ".bak"
    )
    if not os.path.exists(original_path):
        return True
    if not original_path or original_path == backup_path:
        print("Invalid backup file path.")
        return False
    try:
        if os.path.exists(original_path):
            os.remove(original_path)
        shutil.copyfile(backup_path, original_path)
        return True
    except IOError as e:
        log.error(
            "failed to restore file",
            backup_path=backup_path,
            original_path=original_path,
            exc_info=e,
        )
        return False


def test_nginx():
    try:
        subprocess.run(["nginx", "-t"], check=True, text=True, capture_output=True)
        return True
    except subprocess.CalledProcessError as e:
        log.error(
            "nginx test failed",
            exc_info=e,
        )
        return False


def reload_nginx():
    try:
        subprocess.run(
            ["systemctl", "reload", "nginx"], check=True, text=True, capture_output=True
        )
        return True
    except subprocess.CalledProcessError as e:
        log.error(
            "nginx reload failed",
            exc_info=e,
        )
        return False


def fetch_ips(registry) -> List[str]:
    g_cloudfront = Gauge(
        "nginx_trusted_proxy_updater_cloudfront_ok",
        "1 of the updater can fetch aws cloudfront ips",
        registry=registry,
    )

    g_cloudflare = Gauge(
        "nginx_trusted_proxy_updater_cloudflare_ok",
        "1 of the updater can fetch aws cloudflare ips",
        registry=registry,
    )
    cloudfront_ranges = validate_cidrs(get_cloudfront_ip_ranges())
    if cloudfront_ranges is not None and len(cloudfront_ranges) > 0:
        g_cloudfront.set(1)
    else:
        g_cloudfront.set(0)

    cloudflare_ranges = validate_cidrs(get_cloudflare_ip_ranges())
    if cloudflare_ranges is not None and len(cloudflare_ranges) > 0:
        g_cloudflare.set(1)
    else:
        g_cloudflare.set(0)

    return cloudfront_ranges + cloudflare_ranges


def main():
    registry = CollectorRegistry()

    g_ok = Gauge(
        "nginx_trusted_proxy_updater_nginx_ok",
        "1 of the updater can succesfully update the nginx config",
        registry=registry,
    )

    try:
        log.info("fetching ips")
        ip_list = fetch_ips(registry)
        log.info("formatting nginx conf")
        nginx_conf_str = format_nginx_conf(ip_list)
        log.info("backing up existing conf")
        ok = backup_file(NGINX_PROXY_CONF_PATH)
        if not ok:
            sys.exit(1)
        log.info("writing conf")
        ok = write_nginx_conf(NGINX_PROXY_CONF_PATH, nginx_conf_str)
        if not ok:
            sys.exit(1)
        ok = test_nginx()
        log.info("testing nginx")
        if not ok:
            log.info("restoring original conf")
            restore_file(NGINX_PROXY_CONF_PATH)
            sys.exit(1)
        ok = reload_nginx()
        log.info("reloading nginx")
        if not ok:
            log.info("restoring original conf")
            restore_file(NGINX_PROXY_CONF_PATH)
            reload_nginx()
            sys.exit(1)
        g_ok.set(1)
        log.info("all done")

    except Exception as e:
        log.error(
            "unhandled exception",
            exc_info=e,
        )
    finally:
        write_prom_metrics(registry)


if __name__ == "__main__":
    main()
