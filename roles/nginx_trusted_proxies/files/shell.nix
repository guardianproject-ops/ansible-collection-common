{ system ? "x86_64-linux", pkgs ? import <nixpkgs> { inherit system; } }:

let
  packages = [
    pkgs.python311
    pkgs.python311Packages.prometheus-client
    pkgs.python311Packages.requests 
    pkgs.python311Packages.structlog
    pkgs.zsh
  ];
in
  pkgs.mkShell {
    buildInputs = packages;
    shellHook = ''
      export SHELL=${pkgs.zsh}
      export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    '';
  }
