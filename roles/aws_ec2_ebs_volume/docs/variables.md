## Role Variables

* `aws_ec2_ebs_volume_disk_dev`: `/dev/nvme1n1` - the /dev/XXX identifier for the disk



* `aws_ec2_ebs_volume_fstype`: `ext4` - the fstype the disk should be formatted with



* `aws_ec2_ebs_volume_mountpoint`: `/var/lib/application` - the mountpoint that the disk should be mounted to


