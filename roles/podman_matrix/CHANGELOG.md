# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.4](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/2.0.3...2.0.4) (2022-02-01)


### Bug Fixes

* specify awslogs or default logging driver, not both ([a5a3b56](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/a5a3b5646cee24411447e4ed0161b3872c48727a))

### [2.0.3](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/2.0.2...2.0.3) (2021-12-14)


### Bug Fixes

* workaround string multiplication issue ([4580929](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/458092931885753658bbd0b59aca9c9996f25d54))

### [2.0.2](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/2.0.1...2.0.2) (2021-12-01)


### Bug Fixes

* reduce default media upload tmpfs to 4096 mb ([76660d9](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/76660d97954672d4f7a05c583017c010adb84a46))

### [2.0.1](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/2.0.0...2.0.1) (2021-11-10)


### Bug Fixes

* Add tmpfs to synapse container ([83ce7a2](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/83ce7a2407aeb341b5708ada54fc5a54cf66c5cc))

### [2.0.1](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/2.0.0...2.0.1) (2021-11-10)


### Bug Fixes

* Add tmpfs to synapse container ([83ce7a2](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/83ce7a2407aeb341b5708ada54fc5a54cf66c5cc))

## [2.0.0](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/compare/1.0.1...2.0.0) (2021-10-12)


### ⚠ BREAKING CHANGES

* Use synapse version 1.44.0

### Bug Fixes

* Use synapse version 1.44.0 ([adb44c5](https://gitlab.com/guardianproject-ops/ansible-docker-matrix/commit/adb44c5697053975ff9984042a5166f67a021aad))

### 1.0.1 (2021-10-11)
