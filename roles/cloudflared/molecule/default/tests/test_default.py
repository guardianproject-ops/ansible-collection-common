import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


@pytest.mark.parametrize(
    "name,content",
    [

        ("/etc/cloudflared/cloudflaretest.yml", 'tunnel: "uuid1"'),
        ("/etc/cloudflared/cloudflaretest.yml", 'credentials-file: "uuid1.json"'),
        ("/etc/cloudflared/cloudflaretest.yml", 'url: http://service1.example.com'),
        ("/etc/cloudflared/cloudflaretest2.yml", 'hostname: barfoo.com'),
        ("/etc/cloudflared/cloudflaretest2.yml", 'tunnel: "uuid2"'),
        ("/etc/cloudflared/cloudflaretest2.yml", 'credentials-file: "uuid2.json"'),
        ("/etc/cloudflared/uuid1.json", '"TunnelID": "uuid1"'),
        ("/etc/cloudflared/uuid2.json", '"TunnelID": "uuid2"')
        ],
)
def test_files(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)


@pytest.mark.parametrize("name", ["cloudflared@cloudflaretest", "cloudflared@cloudflaretest2"])
def test_services(host, name):
    service = host.service(name)
    assert not service.is_running
    assert service.is_enabled
