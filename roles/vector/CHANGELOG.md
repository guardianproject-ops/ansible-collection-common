# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.7.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.6.0...0.7.0) (2021-12-14)


### ⚠ BREAKING CHANGES

* update default vector version

### Features

* update default vector version ([974e616](https://gitlab.com/guardianproject-ops/ansible-vector/commit/974e61691774426efb8d0b237c3a41faee866b14))

## [0.7.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.6.0...0.7.0) (2021-12-14)


### ⚠ BREAKING CHANGES

* update default vector version

### Features

* update default vector version ([974e616](https://gitlab.com/guardianproject-ops/ansible-vector/commit/974e61691774426efb8d0b237c3a41faee866b14))

## [0.6.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.5.0...0.6.0) (2021-10-07)


### ⚠ BREAKING CHANGES

* Upgrade to vector 16.1 by default.

### Features

* Upgrade to vector 16.1 by default. ([a01a734](https://gitlab.com/guardianproject-ops/ansible-vector/commit/a01a7347305f9f0a2a07a0fbb8813fd70c28662d))

## [0.5.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.4.0...0.5.0) (2021-04-14)


### ⚠ BREAKING CHANGES

* upgrade default to vector 0.12.2

### Features

* upgrade default to vector 0.12.2 ([4c66233](https://gitlab.com/guardianproject-ops/ansible-vector/commit/4c66233b6befe5e831845cb6d18cf7ce0d70c8bb))

## [0.4.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.3.0...0.4.0) (2021-03-02)


### ⚠ BREAKING CHANGES

* use new timber.io vector repository

### Features

* use new timber.io vector repository ([403e4d7](https://gitlab.com/guardianproject-ops/ansible-vector/commit/403e4d75ad29b75a80ea393c01d07b684e9e690c))

## [0.3.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.2.0...0.3.0) (2021-01-25)


### ⚠ BREAKING CHANGES

* update vector url and pin version

### Bug Fixes

* update vector url and pin version ([4a4e98b](https://gitlab.com/guardianproject-ops/ansible-vector/commit/4a4e98bfd7cba7f4d267f90e7e9234136555e615))

## [0.2.0](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.1.1...0.2.0) (2020-12-11)


### ⚠ BREAKING CHANGES

* upgrade to 0.11.x and use yaml configuration

### Features

* upgrade to 0.11.x and use yaml configuration ([154aa2f](https://gitlab.com/guardianproject-ops/ansible-vector/commit/154aa2f89d01b405f5b184ba6e7ecc56075202ce))

### [0.1.1](https://gitlab.com/guardianproject-ops/ansible-vector/compare/0.1.0...0.1.1) (2020-09-21)


### Features

* add vector_member_additional_groups input ([810abeb](https://gitlab.com/guardianproject-ops/ansible-vector/commit/810abeb567f4ff96ee8a24dc0f57912cbd20fd36))

## 0.1.0 (2020-09-18)
