import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule-vector")


def test_directories(host):
    dirs = [
        "/etc/vector",
        "/var/lib/vector",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory


def test_files(host):
    files = [
        "/etc/vector/vector.yaml",
        "/etc/default/vector",
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_service(host):
    s = host.service("vector")
    assert s.is_enabled
    assert s.is_running


def test_passwd_file(host):
    user = host.user("vector")
    assert user.exists
    assert "systemd-journal" in user.groups
    assert "adm" in user.groups
    assert "docker" not in user.groups
