# systemd_user_service_manager

This role is based off of [com.devture.ansible.role.systemd_service_manager](https://github.com/devture/com.devture.ansible.role.systemd_service_manager/tree/main), but modified to support systemd user services.
