#!/usr/bin/env python3
from pystemd.dbuslib import DBus
from pystemd.systemd1 import Manager
from pystemd.systemd1 import Unit
import json
import argparse
import sys


def list_services():
    results = {}
    with DBus(user_mode=True) as bus:
        manager = Manager(bus=bus)
        manager.load()
        for u in manager.Manager.ListUnits():
            u_name = u[0].decode("utf-8")
            u_loaded = u[2].decode("utf-8")
            u_active = u[3].decode("utf-8")
            u_active_sub = u[4].decode("utf-8")
            results[u_name] = {
                "name": u_name,
                "state": u_active,
                "sub_state": u_active_sub,
            }

    return results


def find_mismatches(
    service_names, assert_state=None, assert_sub_state=None, assert_exists=False
):
    all_services = list_services()
    mismatches = []

    for service_name in service_names:
        service_info = all_services.get(service_name)

        if assert_exists:
            if not service_info and assert_exists:
                mismatches.append(
                    {"name": service_name, "fail": "assert-exists", "actual": None}
                )
                continue
        elif not service_info:
            continue

        if assert_state and service_info["state"] != assert_state:
            mismatches.append(
                {"name": service_name, "fail": "assert-state", "actual": service_info}
            )
            continue

        if assert_sub_state and service_info["sub_state"] != assert_sub_state:
            mismatches.append(
                {
                    "name": service_name,
                    "fail": "assert-sub-state",
                    "actual": service_info,
                }
            )

    return mismatches


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("service_names", type=str, help="JSON list of service names")
    parser.add_argument("--assert-state", type=str, help="Assert state")
    parser.add_argument("--assert-sub-state", type=str, help="Assert sub state")
    parser.add_argument("--assert-exists", action="store_true", help="Assert exists")

    args = parser.parse_args()
    service_names = json.loads(args.service_names)

    mismatches = find_mismatches(
        service_names, args.assert_state, args.assert_sub_state, args.assert_exists
    )
    print(json.dumps(mismatches))


if __name__ == "__main__":
    main()
