---
- name: Ensure Grafana paths exists
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: 0750
    owner: "{{ mon_user_username }}"
    group: "{{ mon_user_groupname }}"
  with_items:
    - "{{ mon_grafana_base_path }}"
    - "{{ mon_grafana_config_path }}"
    - "{{ mon_grafana_config_path }}/dashboards"
    - "{{ mon_grafana_config_path }}/provisioning"
    - "{{ mon_grafana_config_path }}/provisioning/dashboards"
    - "{{ mon_grafana_config_path }}/provisioning/datasources"
    - "{{ mon_grafana_data_path }}"
  tags:
    - setup-grafana
    - setup-all

- name: Ensure Grafana config installed
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ mon_grafana_config_path }}/{{ item.dest }}"
    owner: "{{ mon_user_username }}"
    group: "{{ mon_user_groupname }}"
    mode: 0600
  loop:
    - src: grafana/provisioning/dashboards/files.yml
      dest: provisioning/dashboards/files.yml
    - src: grafana/provisioning/datasources/prometheus.yml
      dest: provisioning/datasources/prometheus.yml
    - src: grafana/provisioning/datasources/alertmanager.yml.j2
      dest: provisioning/datasources/alertmanager.yml
  tags:
    - setup-grafana
    - setup-all

- name: Define Grafana quadlet
  ansible.builtin.template:
    src: grafana.container.j2
    dest: "{{ mon_quadlet_dir }}/grafana.container"
    owner: "{{ mon_user_username }}"
    group: "{{ mon_user_groupname }}"
    mode: 0600
  tags:
    - setup-grafana
    - setup-all

- name: Define Grafana cf access proxy quadlet
  ansible.builtin.template:
    src: grafana-cfaccess.container.j2
    dest: "{{ mon_quadlet_dir }}/grafana-cfaccess.container"
    owner: "{{ mon_user_username }}"
    group: "{{ mon_user_groupname }}"
    mode: 0600
  tags:
    - setup-grafana
    - setup-all

- name: Download external grafana dashboards
  ansible.builtin.get_url:
    url: "{{ item.url }}"
    dest: "{{ mon_grafana_config_path }}/dashboards/{{ item.name }}.json"
    mode: 0644
  loop: "{{ mon_grafana_external_dashboards }}"
  tags:
    - setup-grafana
    - setup-all

- name: Ensure Grafana Labs dashboards installed
  ansible.builtin.get_url:
    url: "https://grafana.com/api/dashboards/{{ item.gnetId }}/revisions/{% if item.revision %}{{ item.revision }}{% else %}1{% endif %}/download"
    dest: "{{ mon_grafana_config_path }}/dashboards/{{ item.name }}.json"
    mode: 0644
  loop: "{{ mon_grafana_labs_dashboards }}"
  tags:
    - setup-grafana
    - setup-all

- name: Replace datasource in dashboard content
  ansible.builtin.replace:
    path: "{{ mon_grafana_config_path }}/dashboards/{{ item.name }}.json"
    regexp: '("datasource":)[^,]*,'
    replace: '\1 "{{ item.datasource }}",'
  when:
    - item.datasource is defined and item.datasource != ''
  loop: "{{ mon_grafana_labs_dashboards }}"
  tags:
    - setup-grafana
    - setup-all

- name: Ensure Grafana dashboards installed
  ansible.builtin.copy:
    src: "grafana/dashboards/{{ item.file }}"
    dest: "{{ mon_grafana_config_path }}/dashboards/{{ item.file }}"
  loop: "{{ mon_grafana_dashboards }}"
  tags:
    - setup-grafana
    - setup-all

- name: Ensure container image is pulled
  containers.podman.podman_image:
    name: "{{ mon_grafana_image }}"
  environment:
    XDG_RUNTIME_DIR: "/run/user/{{ mon_user_uid }}"
  tags:
    - setup-grafana
    - setup-all
