# yaml-language-server: $schema=https://raw.githubusercontent.com/ansible/ansible-lint/main/src/ansiblelint/schemas/playbook.json
---
- name: Deploy infrastructure resources with Terraform to AWS
  hosts: monitoring
  gather_facts: false
  tags: infra
  vars:
    terraform_module_source: "{{ terraform_module_source_monitoring_stack }}"
    terraform_s3_backend_key: monitoring-stack
    terraform_aws_provider_extras:
      - alias: us_east_1
        region: us-east-1
  environment:
    AWS_PROFILE: "{{ aws_profile }}"
    AWS_REGION: "{{ aws_region }}"
  tasks:
    - ansible.builtin.import_tasks: "{{ playbook_dir }}/tasks/terraform_backend.yml"
      tags: [always]

    - name: Store KMS Key for deployment
      delegate_to: localhost
      ansible.builtin.set_fact:
        deployment_kms_key_arn: "{{ _terraform_sops_kms_key.key_arn }}"

    - name: Infra
      delegate_to: localhost
      ansible.builtin.import_role:
        name: sr2c.terraform.terraform_module

    - name: Save Terraform outputs for later
      delegate_to: localhost
      ansible.builtin.set_fact:
        terraform_persisted_outputs:
          main_instance_id: "{{ terraform_outputs.main_instance_id }}"
          vpc_id: "{{ terraform_outputs.vpc_id }}"
          vpc_cidr_block: "{{ terraform_outputs.vpc_cidr_block }}"
          log_group_main_arn: "{{ terraform_outputs.log_group_main_arn }}"
          log_group_main_name: "{{ terraform_outputs.log_group_main_name }}"
          monitoring_tunnel_id: "{{ terraform_outputs.tunnel_id }}"
          monitoring_tunnel_token: "{{ terraform_outputs.tunnel_token }}"
          main_instance_tailnet_key: "{{ terraform_outputs.main_instance_tailnet_key }}"
          cloudflare_access_application_id: "{{ terraform_outputs.cloudflare_access_application_id }}"
          cloudflare_access_application_audience: "{{ terraform_outputs.cloudflare_access_application_audience }}"
          cloudwatch_cross_account_roles: "{{ terraform_outputs.cloudwatch_cross_account_roles }}"
          rds_admin_username: "{{ terraform_outputs.rds_admin_username }}"
          db_instance_address: "{{ terraform_outputs.db_instance_address }}"
          db_instance_arn: "{{ terraform_outputs.db_instance_arn }}"
          db_instance_master_user_secret_arn: "{{ terraform_outputs.db_instance_master_user_secret_arn }}"
          db_instance_master_user_secret_name: "{{ terraform_outputs.db_instance_master_user_secret_name }}"
          sns_topic_arns_alarms: "{{ terraform_outputs.sns_topic_arns_alarms }}"
          sns_topics_alarms: "{{ terraform_outputs.sns_topics_alarms }}"

    - ansible.builtin.import_tasks: "{{ playbook_dir }}/tasks/terraform_output_sops_encrypt.yml"

    - name: Save Cloudflare Group Terraform outputs for later
      delegate_to: localhost
      community.sops.sops_encrypt:
        path: "{{ sops_cloudflare_generated_secrets }}"
        kms: "{{ gpops_sops_kms_key_arn }}"
        aws_profile: "{{ gpops_sops_aws_profile }}"
        age: "{{ sops_keys_age }}" # offline disaster recovery
        content_yaml:
          cloudflare_access_service_token_grafana_client_id: "{{ terraform_outputs.cloudflare_access_service_token_grafana_client_id }}"
          cloudflare_access_service_token_grafana_client_secret: "{{ terraform_outputs.cloudflare_access_service_token_grafana_client_secret }}"
