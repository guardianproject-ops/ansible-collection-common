---
# yaml-language-server: $schema=https://raw.githubusercontent.com/ansible/ansible-lint/main/src/ansiblelint/schemas/playbook.json
- name: Deploy infrastructure resources with Terraform to AWS
  hosts: many_mirrors
  gather_facts: false
  tags: infra
  vars:
    common_terraform_inputs:
      namespace: "{{ namespace }}"
      tenant: "{{ tenant }}"
      stage: "{{ stage }}"
      label_order: ["namespace", "environment", "tenant", "stage", "name", "attributes"]
      kms_key_arn: "{{ deployment_kms_key_arn }}"
      providers:
        aws.us_east_1: aws.us_east_1
        aws.gp_operations: aws.gp_operations
        aws: aws
    module_extra:
      provider:
        - aws:
            alias: gp_operations
            region: eu-west-1
            profile: gp-operations
        - aws:
            alias: us_east_1
            region: us-east-1
        - aws:
            region: "{{ aws_region }}"
  environment:
    AWS_PROFILE: "{{ aws_profile }}"
    AWS_REGION: "{{ aws_region }}"
  tasks:

    - ansible.builtin.import_tasks: "{{ playbook_dir }}/tasks/terraform_backend.yml"
      tags: [always]

    - name: Store KMS Key for deployment
      delegate_to: localhost
      ansible.builtin.set_fact:
        deployment_kms_key_arn: "{{ _terraform_sops_kms_key.key_arn }}"
        _terraform_module_backend_key: "{{ namespace }}-{{ tenant }}-{{ stage }}-many-mirrors"

    - name: Matrix Synapse Infra
      delegate_to: localhost
      ansible.builtin.import_role:
        name: sr2c.terraform.terraform_module
      tags: [aws]
      vars:
        terraform_module_backend_config:
          s3: "{{ terraform_s3_backend_config | combine({'key': _terraform_module_backend_key}) }}"
        terraform_module_source: "{{ terraform_module_source_many_mirrors }}"
        terraform_module_inputs: '{{ common_terraform_inputs | ansible.builtin.combine(aws_infra_inputs) }}'
        terraform_module_extra: '{{ module_extra }}'
        terraform_module_outputs_var: "terraform_outputs_many_mirrors"

    - name: Save Terraform outputs for later
      delegate_to: localhost
      community.sops.sops_encrypt:
        path: "{{ sops_deployment_secrets }}"
        kms: "{{ gpops_sops_kms_key_arn }}"
        aws_profile: "{{ gpops_sops_aws_profile }}"
        age: "{{ sops_keys_age }}" # offline disaster recovery
        content_yaml:
          mirrors: "{{ terraform_outputs_many_mirrors.mirrors }}"
          cert_id: "{{ terraform_outputs_many_mirrors.cert_id }}"
          cert_arn: "{{ terraform_outputs_many_mirrors.cert_arn }}"
          cert_alias_cnames: "{{ terraform_outputs_many_mirrors.alias_cnames }}"
