from ansible.plugins.inventory import BaseInventoryPlugin, Constructable
from ansible.inventory.group import to_safe_group_name
from ansible.module_utils._text import to_text
import json
import platform
import subprocess

ANSIBLE_METADATA = {
    'metadata_version': '1.0.0',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: tailscale
plugin_type: inventory
short_description: Fetch dynamic inventory from tailscale
version_added: "0.0.1"
description:
    - "Fetch dynamic inventory from tailscale"
options:
author:
    - Abel Luck <abel@guardianproject.info>
extends_documentation_fragment:
  - constructed
'''

class InventoryModule(BaseInventoryPlugin, Constructable):
    NAME = "gpops.keanu.tailscale"

    @staticmethod
    def custom_sanitizer(name):
        return to_safe_group_name(name, replacer='')

    def verify_file(self, path):
        return True

    def _sanitize_hostname(self, hostname):
        if ':' in to_text(hostname):
            return self._sanitize_group_name(to_text(hostname))
        else:
            return to_text(hostname)


    def _populate(self, hosts):
        for host in hosts:
            for tag in host["tags"]:
                group = self.inventory.add_group(tag)
                self._add_hosts(hosts=groups[group], group=group, hostnames=hostnames)
                self.inventory.add_child('all', group)

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)
        self._sanitize_group_name = custom_sanitizer

        results = self._query()
        self._populate(results)

   def _query(self):

        if platform.system() == "Linux":
            tailscale_cmd = "tailscale"
        elif platform.system() == "Darwin":
            tailscale_cmd = "/Applications/Tailscale.app/Contents/MacOS/Tailscale"

        tailscale_proc = subprocess.run(
            [tailscale_cmd, "status", "--self", "--json"],
            capture_output=True,
        )
        tailscale_output_json = json.loads(tailscale_proc.stdout)

        all_hosts = list(
            tailscale_output_json["Peer"].values(),
        )
        all_hosts.append(
            tailscale_output_json["Self"],
        )

        hosts = []
        for v in all_hosts:
            host = {}
            host["hostname"]  =  v["HostName"]
            host["dnsname"] = v["DNSName"]
            host["tailscale_ips"] = v["TailscaleIPs"]
            tags = []
            if "Tags" in v:
                for tag_str in v["Tags"]:
                    _, tag_name = tag_str.split(":")
                    tags.append(tag_name)
            host["tags"] = tags
            host["online"] = True if v["Online"] else False
            host["os"] = v["OS"]
        return hosts
