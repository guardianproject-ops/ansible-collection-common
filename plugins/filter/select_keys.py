from ansible.errors import AnsibleError
from ansible.utils.display import Display

display = Display()


class FilterModule(object):
    """
    Ansible custom filter plugin.
    """

    def filters(self):
        return {
            "select_keys": select_keys,
        }


def select_keys(item, *keys):
    """Select only specified keys from a dictionary."""
    return {key: item.get(key) for key in keys}
