from ansible.errors import AnsibleError
from ansible.utils.display import Display

display = Display()

# Given a list like
#  - alias: gp_operations
#    profile: gp-operations
#    region: eu-west-1
# Produces a dict like
#   aws.gp_operations: aws.gp_operations
#   aws: aws


class FilterModule(object):
    def filters(self):
        return {
            "to_aws_terraform_provider_inputs": self.to_aws_terraform_provider_inputs
        }

    def to_aws_terraform_provider_inputs(self, providers):
        result = {"aws": "aws"}

        # Count default providers (those without alias)
        default_providers = sum(1 for p in providers if "alias" not in p)
        if default_providers > 1:
            raise AnsibleError(
                f"Found {default_providers} default AWS providers (without alias). Only one is allowed."
            )

        for provider in providers:
            if "alias" in provider:
                aws_alias = f"aws.{provider['alias']}"
                result[aws_alias] = aws_alias

        return result
