#!/usr/bin/python
# Copyright: (c) 2024, Your Name <your.email@example.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: aws_control_tower_account
short_description: Manage AWS accounts through Control Tower
version_added: "1.0.0"
description:
    - Create and manage AWS accounts through AWS Control Tower.
    - O(state=present) creates a new account in AWS Control Tower. All account creation parameters are required.
    - O(state=absent) removes an account from AWS Control Tower. Only account_id is required.
    - Operations are asynchronous and may take 15-30 minutes to complete.
options:
    state:
        description:
            - Whether the account should exist or not.
            - O(state=present) ensures the account exists in Control Tower.
            - O(state=absent) ensures the account is removed from Control Tower.
        type: str
        required: true
        choices: ['present', 'absent']
    account_name:
        description:
            - Name of the AWS account.
            - Must be unique within your organization.
            - Used as the primary identifier for the account.
            - Required when O(state=present)
        type: str
        required: false
    account_id:
        description:
            - ID of the AWS account to delete.
            - Required when O(state=absent)
        type: str
        required: false
    account_email:
        description:
            - Email address for the AWS account.
            - Must be unique across all AWS accounts.
            - Required when O(state=present).
        type: str
        required: false
    organizational_unit:
        description:
            - Name of the Organizational Unit (OU) where the account should be created.
            - Must be an existing OU in Control Tower.
            - Required when O(state=present).
        type: str
        required: false
    sso_user_email:
        description:
            - Email address for the SSO user.
            - Can be the same as O(account_email).
            - Required when O(state=present).
        type: str
        required: false
    sso_user_first_name:
        description:
            - First name of the SSO user.
            - Required when O(state=present).
        type: str
        required: false
    sso_user_last_name:
        description:
            - Last name of the SSO user.
            - Required when O(state=present).
        type: str
        required: false
    close_account_on_delete:
        description:
            - Whether or not to close (terminate!) the AWS account when O(state=absent)
            - Required when O(state=absent).
            - If O(close_account_on_delete=true), then the AWS account will unmangaged from Control Tower and closed.
            - If O(close_account_on_delete=false), then the AWS account will unmangaged from Control Tower but not closed.
        type: bool
        default: false
    wait:
        description:
            - Whether to wait for the operation to complete.
            - O(wait=true) will poll until the operation completes or times out.
            - O(wait=false) will return immediately after starting the operation.
        type: bool
        default: true
    wait_timeout:
        description:
            - How long to wait for account operations to complete, in seconds.
            - Only applies when O(wait=true).
        type: int
        default: 1800
author:
    - "Abel Luck"
"""

EXAMPLES = r"""
# Create a new AWS account and wait for completion (default behavior)
- name: Create new AWS account
  gpops.common.aws_control_tower_account:
    state: present
    account_email: "user@example.com"
    account_name: "prod-account-1"
    organizational_unit: "Production"
    sso_user_email: "user@example.com"
    sso_user_first_name: "John"
    sso_user_last_name: "Doe"

# Create account and return immediately
- name: Create new AWS account without waiting
  gpops.common.aws_control_tower_account:
    state: present
    account_email: "user@example.com"
    account_name: "prod-account-2"
    organizational_unit: "Production"
    sso_user_email: "user@example.com"
    sso_user_first_name: "Jane"
    sso_user_last_name: "Smith"
    wait: false

# Run asynchronously using Ansible async
- name: Create new AWS account asynchronously
  gpops.common.aws_control_tower_account:
    state: present
    account_email: "user@example.com"
    account_name: "prod-account-3"
    organizational_unit: "Production"
    sso_user_email: "user@example.com"
    sso_user_first_name: "Bob"
    sso_user_last_name: "Johnson"
  async: 3600
  poll: 30

# Remove an account from Control Tower governance
- name: Remove AWS account
  gpops.common.aws_control_tower_account:
    state: absent
    account_id: "123456"
    close_account_on_delete: false

# Close/terminate an account entirely, and move it to the Suspended OU
- name: Terminate AWS account
  gpops.common.aws_control_tower_account:
    state: absent
    account_name: "prod-account-1"
    account_id: "123456"
    close_account_on_delete: true
    organizational_unit: Suspended
"""

RETURN = r"""
changed:
    description: Whether the account state was changed
    type: bool
    returned: always
    sample: true
account_id:
    description: The AWS account ID (once available)
    type: str
    returned: when account is created successfully
    sample: "123456789012"
record_id:
    description: The Service Catalog record ID for tracking the operation
    type: str
    returned: always
    sample: "rec-abcd1234efgh5678"
operation_status:
    description: 
        - Current status of the operation
        - When wait=true, will be final status (SUCCEEDED/FAILED)
        - When wait=false, likely to be IN_PROGRESS
    type: str
    returned: always
    choices: ["FAILED", "SUCCEEDED", "WAITING", "IN_PROGRESS"]
    sample: "SUCCEEDED"
account_status:
    description:
        - Current status of the account in AWS Org
        - Only returned when wait=true and operation succeeds
    type: str
    returned: when wait=true and success
    choices: ["ACTIVE", "SUSPENDED", "PENDING_CLOSURE"]
    sample: "AVAILABLE"
control_tower_status:
    description:
        - Current status of the account in Control Tower
        - Only returned when wait=true and operation succeeds
    type: str
    returned: when wait=true and success
    choices: ["AVAILABLE", "UNDER_CHANGE", "TAINTED", "ERROR", "PLAN_IN_PROGRESS", "TERMINATED"]
    sample: "AVAILABLE"
message:
    description: Informational message about the operation
    type: str
    returned: always
    sample: "Account creation request for example-account has been submitted."
error:
    description: Error message if operation failed
    type: str
    returned: when operation fails
    sample: "Account creation failed: OU does not exist"
"""

try:
    import botocore
    from botocore.exceptions import BotoCoreError
    from botocore.exceptions import ClientError
    from botocore.exceptions import WaiterError
except ImportError:
    pass  # caught by AnsibleAWSModule


from ansible_collections.gpops.common.plugins.module_utils.aws_waiters import (
    wait_for_record_completion,
    wait_for_provisioned_product,
    wait_for_account_closure,
    wait_for_provisioned_product_termination,
)

try:
    from ansible_collections.gpops.common.plugins.module_utils.aws_avm import (
        AccountVendingMachine,
        AccountStatus,
        ProvisionedProductStatus,
    )
except ImportError:

    class AccountVendingMachine:
        pass

    pass  # caught by GuardianProjectAnsibleAWSModule


from ansible.module_utils._text import to_native
from ansible.module_utils.common.dict_transformations import camel_dict_to_snake_dict
from ansible_collections.amazon.aws.plugins.module_utils.tagging import (
    ansible_dict_to_boto3_tag_list,
)
from ansible_collections.gpops.common.plugins.module_utils.modules import (
    GuardianProjectAWSModule as AnsibleAWSModule,
)
from ansible_collections.amazon.aws.plugins.module_utils.retries import AWSRetry


def ensure_account_present(module: AnsibleAWSModule, avm: AccountVendingMachine):
    """
    Ensure an AWS account exists and is managed by Control Tower.

    Args:
        module: The AnsibleAWSModule instance

    Returns:
        dict: Result dictionary with operation status and details
    """
    result = dict(
        changed=False,
        account_id=None,
        record_id=None,
        operation_status=None,
        account_status=None,
        control_tower_status=None,
        message=None,
        error=None,
    )
    account_name = module.params["account_name"]

    try:
        existing_account = avm.get_org_account_by_name(account_name)
        existing_provision = avm.get_provisioned_product_by_account_name(account_name)

        if existing_account and existing_provision:
            # Account exists and is managed by Control Tower
            result["account_id"] = existing_account.account_id
            result["message"] = (
                f"Account {account_name} already exists and is managed by Control Tower"
            )
            result["account_status"] = existing_account.status.value
            result["control_tower_status"] = existing_provision.status.value
            return result

        if existing_account and not existing_provision:
            result["account_id"] = existing_account.account_id
            result["account_status"] = existing_account.status.value
            # Account exists but isn't managed by Control Tower
            module.fail_json(
                msg=f"Account {account_name} exists but is not managed by Control Tower. This module does not support enrolling existing accounts.",
                account_id=existing_account.account_id,
                **result,
            )

        if not existing_account and existing_provision:
            # Provisioning might be in progress or failed
            provision_status = avm.get_provisioned_product_status(existing_provision.id)
            result["record_id"] = existing_provision.id
            result["control_tower_status"] = provision_status

            if provision_status in ["IN_PROGRESS", "UNDER_CHANGE"]:
                result["message"] = (
                    f"Account creation for {account_name} is still in progress"
                )
                result["operation_status"] = "IN_PROGRESS"
                return result
            result["operation_status"] = provision_status.value
            module.fail_json(
                msg=f"Found provisioned product for {account_name} but no account exists. Previous creation may have failed.",
                **result,
            )

        # No account and no provision exists -> create new account
        record_id = avm.create_control_tower_account(
            account_name=account_name,
            account_email=module.params["account_email"],
            ou_name=module.params["organizational_unit"],
            sso_user_email=module.params["sso_user_email"],
            sso_user_first_name=module.params["sso_user_first_name"],
            sso_user_last_name=module.params["sso_user_last_name"],
        )

        result["record_id"] = record_id
        result["changed"] = True
        if module.params["wait"]:
            # First we have to wait for the record completion
            provisioned_product_id = wait_for_record_completion(module, record_id)

            # ...then wait for the actual account to be ready (or fail)
            wait_for_provisioned_product(module, provisioned_product_id)

            final_status = avm.get_provisioned_product_status(provisioned_product_id)
            result["operation_status"] = (
                final_status.value if final_status else "FAILED"
            )

            result["control_tower_status"] = final_status.value
            if final_status is ProvisionedProductStatus.AVAILABLE:
                account_id = avm.get_account_id_by_provisioned_product_id(
                    provisioned_product_id
                )
                result["account_id"] = account_id
                new_account = avm.get_org_account_by_name(account_name)
                result["account_status"] = new_account.status.value

        else:
            result["operation_status"] = "IN_PROGRESS"
            result["message"] = f"Account creation initiated for {account_name}"

    except Exception as e:
        result["error"] = str(e)
        module.fail_json(msg=str(e), **result)

    return result


def ensure_account_absent(module: AnsibleAWSModule, avm: AccountVendingMachine):
    result = dict(
        changed=False,
        account_id=None,
        record_id=None,
        operation_status=None,
        account_status=None,
        control_tower_status=None,
        message=None,
        error=None,
    )

    account_id = module.params["account_id"]
    close_account = module.params["close_account_on_delete"]

    try:
        existing_account = avm.get_org_account_by_id(account_id)
        if (
            existing_account
            and module.params["account_name"]
            and len(module.params["account_name"]) > 0
            and existing_account.account_name != module.params.get("account_name")
        ):
            # sanity check for the user
            module.fail_json(
                msg=f'Parameter account_name "{module.params["account_name"]}" does not match the account name returned by the AWS API "{existing_account.account_name}"'
            )

        is_suspended = existing_account.status == AccountStatus.SUSPENDED
        existing_provision = avm.get_provisioned_product_by_account_id(
            existing_account.account_id
        )

        if not existing_account and not existing_provision:
            result["message"] = f"Account {account_id} does not exist"
            return result

        if not is_suspended and existing_provision:
            if existing_provision.status in [
                ProvisionedProductStatus.UNDER_CHANGE,
                ProvisionedProductStatus.PLAN_IN_PROGRESS,
            ]:
                module.fail_json(
                    msg=str(
                        f"Cannot terminate provisioned product for {existing_provision.id} for account {account_id} because it is under change. Try again later."
                    ),
                    **result,
                )

            record_id = avm.terminate_provisioned_product(existing_provision.id)
            result["record_id"] = record_id
            result["changed"] = True
            result["message"] = (
                f"Account {account_id}'s is being unmanaged by Control Tower"
            )

            if module.params["wait"]:
                wait_for_record_completion(module, result["record_id"])
                # then we wait for the provisioned product itself to be terminated, this takes awhile
                module.debug("Waiting for account to be unmanaged")
                wait_for_provisioned_product_termination(module, existing_provision.id)
                result["control_tower_status"] = "TERMINATED"
        else:
            result["control_tower_status"] = "TERMINATED"

        if existing_account:
            account_name = existing_account.account_name
            result["account_id"] = existing_account.account_id
            if existing_account.status != AccountStatus.ACTIVE:
                result["account_status"] = existing_account.status.value
                result["message"] = (
                    f"Account {account_name} ({existing_account.account_id}) already suspended or pending closure."
                )
            elif close_account:
                avm.close_account(existing_account.account_id)
                result["changed"] = True
                result["message"] = (
                    f"Account {account_name} ({existing_account.account_id}) removed from Control Tower and closed, you have 90 days to re-open it."
                )
                if module.params["wait"]:
                    module.debug("Waiting for account to be closed")
                    wait_for_account_closure(module, existing_account.account_id)
                    account = avm.get_org_account_by_name(account_name)
                    if account:
                        result["account_status"] = account.status.value
            else:
                result["account_status"] = existing_account.status.value
                module.debug(
                    f"Account {account_name} will not be closed because `close_account_on_delete` is false"
                )

            final_ou = module.params["organizational_unit"]
            if final_ou:
                moved = avm.move_account_to_ou(existing_account.account_id, final_ou)
                if moved:
                    result["changed"] = True

    except Exception as e:
        module.fail_json_aws(e, msg=f"Failed to remove account {account_id}", **result)

    return result


def run_module():
    argument_spec = dict(
        state=dict(type="str", required=True, choices=["present", "absent"]),
        account_name=dict(type="str", required=False),
        account_id=dict(type="str", required=False),
        account_email=dict(type="str", required=False),
        organizational_unit=dict(type="str", required=False),
        sso_user_email=dict(type="str", required=False),
        sso_user_first_name=dict(type="str", required=False),
        sso_user_last_name=dict(type="str", required=False),
        wait=dict(type="bool", required=False, default=True),
        wait_timeout=dict(type="int", required=False, default=1800),
        close_account_on_delete=dict(type="bool", default=False),
    )

    result = dict(
        changed=False,
        account_id=None,
        record_id=None,
        operation_status=None,
        account_status=None,
        control_tower_status=None,
        message=None,
        error=None,
    )

    module = AnsibleAWSModule(
        argument_spec=argument_spec,
        required_if=[
            (
                "state",
                "absent",
                ["account_id", "close_account_on_delete"],
            ),
            (
                "state",
                "present",
                [
                    "account_name",
                    "account_email",
                    "organizational_unit",
                    "sso_user_email",
                    "sso_user_first_name",
                    "sso_user_last_name",
                ],
            ),
        ],
        supports_check_mode=True,
    )

    if module.check_mode:
        module.exit_json(**result)

    try:
        retry_decorator = AWSRetry.jittered_backoff()
        org_client = module.client("organizations", retry_decorator=retry_decorator)
        sc_client = module.client("servicecatalog", retry_decorator=retry_decorator)
        avm = AccountVendingMachine(
            organizations_client=org_client,
            service_catalog_client=sc_client,
            module=module,
        )

        if module.params["state"] == "present":
            module.debug("Ensuring account present")
            result = ensure_account_present(module, avm)
        else:
            result = ensure_account_absent(module, avm)

    except Exception as e:
        module.fail_json(msg=str(e), **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
