# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = r"""
---
module: aws_control_tower_control
short_description: Manage AWS Control Tower controls (guardrails)
version_added: "1.0.0"
description:
    - Enable or disable AWS Control Tower controls (guardrails) for organizational units.
    - Supports waiting for operations to complete.
    - Operations are asynchronous and may take several minutes to complete.
options:
    state:
        description:
            - Whether the control should be enabled or disabled.
            - O(state=enabled) ensures the control is enabled for the specified OU.
            - O(state=disabled) ensures the control is disabled for the specified OU.
        type: str
        required: true
        choices: ['enabled', 'disabled']
    control_arn:
        description:
            - The ARN of the Control Tower control to manage.
            - Must be a valid Control Tower control ARN.
        type: str
        required: true
    organizational_unit_arn:
        description:
            - The ARN of the organizational unit where the control should be managed.
            - Must be a valid OU ARN in your AWS Organization.
        type: str
        required: true
    parameters:
        description:
            - List of parameters to pass to the control when enabling it.
            - Each parameter must have a 'key' and 'value' field.
            - Ignored when O(state=disabled).
        type: list
        elements: dict
        required: false
        suboptions:
            key:
                description: The parameter key name
                type: str
                required: true
            value:
                description: The parameter value or list of values
                type: raw
                required: true
    wait:
        description:
            - Whether to wait for the operation to complete.
            - O(wait=true) will poll until the operation completes or times out.
            - O(wait=false) will return immediately after starting the operation.
        type: bool
        default: true
    wait_timeout:
        description:
            - How long to wait for control operations to complete, in seconds.
            - Only applies when O(wait=true).
        type: int
        default: 300
author:
    - "Your Name"
"""

EXAMPLES = r"""
# Enable a control with parameters
- name: Enable audit bucket policy changes control
  gpops.common.aws_control_tower_control:
    state: enabled
    control_arn: "arn:aws:controltower:us-east-1::control/AWS-GR_AUDIT_BUCKET_POLICY_CHANGES_PROHIBITED"
    organizational_unit_arn: "arn:aws:organizations::123456789012:ou/o-abcd1234/ou-def1-gh2ij3kl"
    parameters:
      - key: ExemptedPrincipalArns
        value: ["arn:aws:iam::*:role/AWSControlTowerExecution"]

# Disable a control
- name: Disable audit bucket policy changes control
  gpops.common.aws_control_tower_control:
    state: disabled
    control_arn: "arn:aws:controltower:us-east-1::control/AWS-GR_AUDIT_BUCKET_POLICY_CHANGES_PROHIBITED"
    organizational_unit_arn: "arn:aws:organizations::123456789012:ou/o-abcd1234/ou-def1-gh2ij3kl"

# Enable control without waiting
- name: Enable control and return immediately
  gpops.common.aws_control_tower_control:
    state: enabled
    control_arn: "arn:aws:controltower:us-east-1::control/AWS-GR_AUDIT_BUCKET_ENCRYPTION_ENABLED"
    organizational_unit_arn: "arn:aws:organizations::123456789012:ou/o-abcd1234/ou-def1-gh2ij3kl"
    wait: false
"""

RETURN = r"""
changed:
    description: Whether the control state was changed
    type: bool
    returned: always
    sample: true
operation_id:
    description: The Control Tower operation identifier
    type: str
    returned: when operation is initiated
    sample: "a1b2c3d4-5678-90ef-ghij-klmnopqrstuv"
operation_status:
    description:
        - Current status of the operation
        - When wait=true, will be final status (SUCCEEDED/FAILED)
        - When wait=false, will be IN_PROGRESS
    type: str
    returned: always
    choices: ["SUCCEEDED", "FAILED", "IN_PROGRESS"]
    sample: "SUCCEEDED"
message:
    description: Informational message about the operation
    type: str
    returned: always
    sample: "Control enabled successfully"
error:
    description: Error message if operation failed
    type: str
    returned: when operation fails
    sample: "Failed to enable control: InvalidParameterException"
"""

try:
    import botocore
    from botocore.exceptions import BotoCoreError, ClientError
    from botocore.waiter import WaiterModel, create_waiter_with_client
except ImportError:
    pass  # caught by AnsibleAWSModule

from ansible.module_utils._text import to_native
from ansible_collections.amazon.aws.plugins.module_utils.retries import AWSRetry
from ansible_collections.gpops.common.plugins.module_utils.modules import (
    GuardianProjectAWSModule as AnsibleAWSModule,
)


def create_control_operation_waiter(client, operation_id, max_attempts=60, delay=5):
    """Create a custom waiter for Control Tower operations."""
    waiter_config = {
        "version": 2,
        "waiters": {
            "ControlOperationComplete": {
                "operation": "GetControlOperation",
                "delay": delay,
                "maxAttempts": max_attempts,
                "acceptors": [
                    {
                        "matcher": "path",
                        "expected": "IN_PROGRESS",
                        "argument": "controlOperation.status",
                        "state": "retry",
                    },
                    {
                        "matcher": "path",
                        "expected": "SUCCEEDED",
                        "argument": "controlOperation.status",
                        "state": "success",
                    },
                    {
                        "matcher": "path",
                        "expected": "FAILED",
                        "argument": "controlOperation.status",
                        "state": "failure",
                    },
                ],
            }
        },
    }

    waiter_model = WaiterModel(waiter_config)
    custom_waiter = create_waiter_with_client(
        "ControlOperationComplete", waiter_model, client
    )
    custom_waiter.wait(operationIdentifier=operation_id)


def check_control_status(client, ou_arn, control_arn):
    """Check if a control is enabled for an OU."""
    try:
        response = client.list_enabled_controls(targetIdentifier=ou_arn)
        enabled_controls = response.get("enabledControls", [])
        return any(
            control["controlIdentifier"] == control_arn for control in enabled_controls
        )
    except (BotoCoreError, ClientError) as e:
        raise e


def enable_control(client, ou_arn, control_arn, parameters=None):
    """Enable a Control Tower control."""
    try:
        kwargs = {
            "controlIdentifier": control_arn,
            "targetIdentifier": ou_arn,
        }
        if parameters:
            kwargs["parameters"] = parameters
        response = client.enable_control(**kwargs)
        return response
    except (BotoCoreError, ClientError) as e:
        raise e


def disable_control(client, ou_arn, control_arn):
    """Disable a Control Tower control."""
    try:
        response = client.disable_control(
            controlIdentifier=control_arn,
            targetIdentifier=ou_arn,
        )
        return response
    except (BotoCoreError, ClientError) as e:
        raise e


def run_module():
    argument_spec = dict(
        state=dict(type="str", required=True, choices=["enabled", "disabled"]),
        control_arn=dict(type="str", required=True),
        organizational_unit_arn=dict(type="str", required=True),
        parameters=dict(
            type="list",
            elements="dict",
            required=False,
            options=dict(
                key=dict(type="str", required=True),
                value=dict(type="raw", required=True),
            ),
        ),
        wait=dict(type="bool", default=True),
        wait_timeout=dict(type="int", default=300),
    )

    module = AnsibleAWSModule(
        argument_spec=argument_spec,
        supports_check_mode=True,
    )

    state = module.params["state"]
    control_arn = module.params["control_arn"]
    ou_arn = module.params["organizational_unit_arn"]
    parameters = module.params.get("parameters")

    result = dict(
        changed=False,
        operation_id=None,
        operation_status=None,
        message=None,
        error=None,
    )

    try:
        retry_decorator = AWSRetry.jittered_backoff()
        client = module.client("controltower", retry_decorator=retry_decorator)

        control_enabled = check_control_status(client, ou_arn, control_arn)

        if state == "enabled" and not control_enabled:
            if not module.check_mode:
                response = enable_control(client, ou_arn, control_arn, parameters)
                result["operation_id"] = response["operationIdentifier"]
                result["changed"] = True

                if module.params["wait"]:
                    create_control_operation_waiter(
                        client,
                        result["operation_id"],
                        max_attempts=module.params["wait_timeout"] // 5,
                    )
                    result["operation_status"] = "SUCCEEDED"
                    result["message"] = f"Control {control_arn} enabled successfully"
                else:
                    result["operation_status"] = "IN_PROGRESS"
                    result["message"] = (
                        f"Control {control_arn} enable operation initiated"
                    )
            else:
                result["changed"] = True
                result["message"] = f"Would enable control {control_arn}"

        elif state == "disabled" and control_enabled:
            if not module.check_mode:
                response = disable_control(client, ou_arn, control_arn)
                result["operation_id"] = response["operationIdentifier"]
                result["changed"] = True

                if module.params["wait"]:
                    create_control_operation_waiter(
                        client,
                        result["operation_id"],
                        max_attempts=module.params["wait_timeout"] // 5,
                    )
                    result["operation_status"] = "SUCCEEDED"
                    result["message"] = f"Control {control_arn} disabled successfully"
                else:
                    result["operation_status"] = "IN_PROGRESS"
                    result["message"] = (
                        f"Control {control_arn} disable operation initiated"
                    )
            else:
                result["changed"] = True
                result["message"] = f"Would disable control {control_arn}"

        else:
            result["message"] = (
                f"Control {control_arn} already in desired state: {state}"
            )

    except Exception as e:
        result["error"] = str(e)
        module.fail_json(msg=str(e), **result)

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
