from ansible_collections.community.aws.plugins.module_utils.base import (
    BaseWaiterFactory,
)

try:
    import botocore
    from botocore.exceptions import BotoCoreError
    from botocore.exceptions import ClientError
    from botocore.exceptions import WaiterError
except ImportError:
    pass  # caught by AnsibleAWSModule


class OrganizationsWaiterFactory(BaseWaiterFactory):  # pylint: disable=R0903
    """
    Facilitates waiting Orgs
    """

    def __init__(self, module):
        client = module.client("organizations")
        super(OrganizationsWaiterFactory, self).__init__(module, client)

    @property
    def _waiter_model_data(self):
        data = super(OrganizationsWaiterFactory, self)._waiter_model_data
        org_data = dict(
            account_closed=dict(
                operation="DescribeAccount",
                delay=30,  # Check every 30 seconds
                maxAttempts=40,  # Up to 20 minutes
                acceptors=[
                    dict(
                        state="success",
                        matcher="path",
                        expected="SUSPENDED",
                        argument="Account.Status",
                    ),
                    dict(
                        state="retry",
                        matcher="path",
                        expected="PENDING_CLOSURE",
                        argument="Account.Status",
                    ),
                ],
            )
        )
        data.update(org_data)
        return data


class ControlTowerWaiterFactory(BaseWaiterFactory):  # pylint: disable=R0903
    """
    Facilitates waiting ControlTower
    """

    def __init__(self, module):
        client = module.client("servicecatalog")
        super(ControlTowerWaiterFactory, self).__init__(module, client)

    @property
    def _waiter_model_data(self):
        data = super(ControlTowerWaiterFactory, self)._waiter_model_data
        sc_data = dict(
            record_completed=dict(
                operation="DescribeRecord",
                delay=30,
                maxAttempts=60,
                acceptors=[
                    dict(
                        state="success",
                        matcher="path",
                        expected="SUCCEEDED",
                        argument="RecordDetail.Status",
                    ),
                    dict(
                        state="failure",
                        matcher="path",
                        expected="FAILED",
                        argument="RecordDetail.Status",
                    ),
                    dict(
                        state="success",
                        matcher="path",
                        expected="IN_PROGRESS",
                        argument="RecordDetail.Status",
                    ),
                    dict(
                        state="success",
                        matcher="path",
                        expected="IN_PROGRESS_IN_ERROR",
                        argument="RecordDetail.Status",
                    ),
                    dict(
                        state="retry",
                        matcher="path",
                        expected="CREATED",
                        argument="RecordDetail.Status",
                    ),
                ],
            ),
            provisioned_product_available=dict(
                operation="DescribeProvisionedProduct",
                delay=30,
                maxAttempts=60,
                acceptors=[
                    dict(
                        state="success",
                        matcher="path",
                        expected="AVAILABLE",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="failure",
                        matcher="path",
                        expected="ERROR",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="retry",
                        matcher="path",
                        expected="UNDER_CHANGE",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="failure",
                        matcher="path",
                        expected="TAINTED",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="retry",
                        matcher="path",
                        expected="PLAN_IN_PROGRESS",
                        argument="ProvisionedProductDetail.Status",
                    ),
                ],
            ),
            provisioned_product_terminated=dict(
                operation="DescribeProvisionedProduct",
                delay=30,
                maxAttempts=60,
                acceptors=[
                    dict(
                        state="success",
                        matcher="error",
                        expected="ResourceNotFoundException",
                    ),
                    dict(
                        state="retry",
                        matcher="path",
                        expected="UNDER_CHANGE",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="failure",
                        matcher="path",
                        expected="ERROR",
                        argument="ProvisionedProductDetail.Status",
                    ),
                    dict(
                        state="failure",
                        matcher="path",
                        expected="TAINTED",
                        argument="ProvisionedProductDetail.Status",
                    ),
                ],
            ),
        )
        data.update(sc_data)
        return data


def wait_for_record_completion(module, record_id):
    """
    Wait for a Service Catalog Record operation to complete.

    Args:
        module: AnsibleAWSModule instance
        record_id: The Record ID to monitor

    Returns:
        provisioned_product_id: The ID of the provisioned product
    """
    try:
        wf = ControlTowerWaiterFactory(module)
        waiter = wf.get_waiter("record_completed")
        waiter.wait(
            Id=record_id, WaiterConfig={"Timeout": module.params["wait_timeout"]}
        )

        # Get the provisioned product ID from the completed record
        response = module.client("servicecatalog").describe_record(Id=record_id)
        return response["RecordDetail"]["ProvisionedProductId"]

    except botocore.exceptions.WaiterError as e:
        module.fail_json_aws(
            e,
            msg=f"Account Provisioning for Service Catalog record id '{record_id}' failed.",
        )


def wait_for_provisioned_product(module, provisioned_product_id):
    """
    Wait for a Provisioned Product to reach AVAILABLE state.

    Args:
        module: AnsibleAWSModule instance
        provisioned_product_id: The Provisioned Product ID to monitor
    """
    try:
        wf = ControlTowerWaiterFactory(module)
        waiter = wf.get_waiter("provisioned_product_available")
        waiter.wait(
            Id=provisioned_product_id,
            WaiterConfig={"Timeout": module.params["wait_timeout"]},
        )
    except botocore.exceptions.WaiterError as e:
        if "ProvisionedProductDetail" in e.last_response:
            status = e.last_response["ProvisionedProductDetail"].get("Status")
            msg = e.last_response["ProvisionedProductDetail"].get("StatusMessage")
            if status == "ERROR" and msg:
                module.fail_json_aws(e, msg=msg)
        module.fail_json_aws(
            e,
            msg=f"Account Provisioning for Product id '{provisioned_product_id}' failed {e.last_response}",
        )


def wait_for_provisioned_product_termination(module, provisioned_product_id):
    """
    Wait for a Provisioned Product to be fully terminated (no longer exists).

    Args:
        module: AnsibleAWSModule instance
        provisioned_product_id: The Provisioned Product ID to monitor
    """
    try:
        wf = ControlTowerWaiterFactory(module)
        waiter = wf.get_waiter("provisioned_product_terminated")
        waiter.wait(
            Id=provisioned_product_id,
            WaiterConfig={"Timeout": module.params["wait_timeout"]},
        )
    except botocore.exceptions.WaiterError as e:
        if "ProvisionedProductDetail" in e.last_response:
            status = e.last_response["ProvisionedProductDetail"].get("Status")
            msg = e.last_response["ProvisionedProductDetail"].get("StatusMessage")
            if status == "ERROR" and msg:
                module.fail_json_aws(e, msg=msg)
        module.fail_json_aws(
            e,
            msg=f"Account termination for Product id '{provisioned_product_id}' failed {e.last_response}",
        )


def wait_for_account_closure(module, account_id):
    """
    Wait for an AWS account to be fully closed.

    Args:
        module: AnsibleAWSModule instance
        account_id: The Account ID to monitor
    """
    try:
        wf = OrganizationsWaiterFactory(module)
        waiter = wf.get_waiter("account_closed")
        waiter.wait(
            AccountId=account_id,
            WaiterConfig={"Timeout": module.params["wait_timeout"]},
        )
    except botocore.exceptions.WaiterError as e:
        module.fail_json_aws(
            e, msg=f"Timeout waiting for account {account_id} to close"
        )
