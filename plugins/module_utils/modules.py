# -*- coding: utf-8 -*-
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from typing import Optional, Dict, Any, Callable
from ansible.module_utils.basic import missing_required_lib
from ansible.module_utils._text import to_native
from ansible_collections.amazon.aws.plugins.module_utils.common import (
    set_collection_info,
)
from ansible_collections.amazon.aws.plugins.module_utils.modules import AnsibleAWSModule
from ansible_collections.amazon.aws.plugins.module_utils.exceptions import (
    AnsibleBotocoreError,
)
from ansible_collections.gpops.common.plugins.module_utils.common import (
    GPOPS_COMMON_COLLECTION_NAME,
)
from ansible_collections.gpops.common.plugins.module_utils.common import (
    GPOPS_COMMON_COLLECTION_VERSION,
)
import traceback

try:
    from mypy_boto3_organizations import OrganizationsClient
    from mypy_boto3_servicecatalog import ServiceCatalogClient

    HAS_MYPY_BOTO3 = True
except ImportError:
    MYPY_BOTO3_IMP_ERR = traceback.format_exc()
    HAS_MYPY_BOTO3 = False


required_stubs = ["mypy-boto3-servicecatalog", "mypy-boto3-organizations"]


def check_sdk_version_supported(
    warn: Optional[Callable] = None,
) -> bool:
    if not HAS_MYPY_BOTO3:
        raise AnsibleBotocoreError(
            message=missing_required_lib(", ".join(required_stubs))
        )


class GuardianProjectAWSModule(AnsibleAWSModule):
    def __init__(self, **kwargs):
        super(GuardianProjectAWSModule, self).__init__(**kwargs)
        set_collection_info(
            collection_name=GPOPS_COMMON_COLLECTION_NAME,
            collection_version=GPOPS_COMMON_COLLECTION_VERSION,
        )
        try:
            check_sdk_version_supported(warn=self.warn)
        except AnsibleBotocoreError as e:
            self._module.fail_json(to_native(e))
