from enum import Enum
import uuid
from typing import Optional, Dict, Any
from dataclasses import dataclass

from mypy_boto3_organizations import OrganizationsClient
from mypy_boto3_servicecatalog import ServiceCatalogClient

try:
    import botocore
    from botocore.exceptions import BotoCoreError
    from botocore.exceptions import ClientError
except ImportError:
    pass  # Handled by AnsibleAWSModule

error_msg_control_tower_missing = "AWS Control Tower Account Factory product not found. Please ensure Control Tower is properly set up in this region."


class ProvisionedProductStatus(Enum):
    AVAILABLE = "AVAILABLE"
    UNDER_CHANGE = "UNDER_CHANGE"
    TAINTED = "TAINTED"
    ERROR = "ERROR"
    PLAN_IN_PROGRESS = "PLAN_IN_PROGRESS"


class RecordStatus(Enum):
    CREATED = "CREATED"
    IN_PROGRESS = "IN_PROGRESS"
    IN_PROGRESS_IN_ERROR = "IN_PROGRESS_IN_ERROR"
    SUCCEEDED = "SUCCEEDED"
    FAILED = "FAILED"


class AccountStatus(Enum):
    ACTIVE = "ACTIVE"
    SUSPENDED = "SUSPENDED"
    PENDING_CLOSURE = "PENDING_CLOSURE"


@dataclass
class Account:
    account_id: str
    account_name: str
    account_org_arn: str
    email: str
    status: AccountStatus


@dataclass
class ProvisionedProduct:
    id: str
    name: str
    status: ProvisionedProductStatus


class AccountVendingMachine:
    """Interface for managing AWS accounts through Control Tower."""

    def __init__(
        self,
        organizations_client: OrganizationsClient,
        service_catalog_client: ServiceCatalogClient,
        module=None,
    ):
        """
        Initialize with boto3 clients.

        Args:
            organizations_client: Boto3 Organizations client from AnsibleAWSModule
            service_catalog_client: Boto3 Service Catalog client from AnsibleAWSModule
        """
        self.org_client = organizations_client
        self.sc_client = service_catalog_client
        self.module = module

    def debug(self, msg):
        if self.module:
            self.module.debug(msg)

    def get_org_account_by_id(self, account_id: str) -> Optional[Account]:
        """
        Get AWS account details by account id.

        Args:
            account_id: Id of the AWS account

        Returns:
            Account object if found, None if not found
        """
        try:
            response = self.org_client.describe_account(AccountId=account_id)
            account = response["Account"]
            return Account(
                account_id=account["Id"],
                account_name=account["Name"],
                account_org_arn=account["Arn"],
                email=account["Email"],
                status=AccountStatus(account["Status"]),
            )
        except self.org_client.exceptions.AccountNotFoundException:
            return None

    def get_org_account_by_name(self, account_name: str) -> Optional[Account]:
        """
        Get AWS account details by account name.

        Args:
            account_name: Name of the AWS account

        Returns:
            Account object if found, None if not found
        """
        paginator = self.org_client.get_paginator("list_accounts")

        for page in paginator.paginate():
            for account in page["Accounts"]:
                if account["Name"] == account_name:
                    return Account(
                        account_id=account["Id"],
                        account_name=account["Name"],
                        account_org_arn=account["Arn"],
                        email=account["Email"],
                        status=AccountStatus(account["Status"]),
                    )

        return None

    def get_account_id_by_provisioned_product_id(
        self, provisioned_product_id: str
    ) -> Optional[str]:
        """
        Get the AWS account ID for a provisioned product.

        Args:
            provisioned_product_id: The ID of the provisioned product

        Returns:
            str: The AWS account ID if found, None otherwise

        Raises:
            Exception: When API call fails
        """
        try:
            response = self.sc_client.get_provisioned_product_outputs(
                aws_retry=True, ProvisionedProductId=provisioned_product_id
            )

            for output in response.get("Outputs", []):
                if output["OutputKey"] == "AccountId":
                    return output["OutputValue"]

            return None

        except (
            botocore.exceptions.ClientError,
            botocore.exceptions.BotoCoreError,
        ) as e:
            raise Exception(
                f"Failed to get account ID for provisioned product {provisioned_product_id}: {str(e)}"
            )

    def get_provisioned_product_by_account_name(
        self, account_name: str
    ) -> Optional[ProvisionedProduct]:
        """
        Get Control Tower provisioned product by account name.

        Args:
            account_name: Name of the AWS account

        Returns:
            ProvisionedProduct if found, None if not found
        """
        try:
            provisioned_name = f"{account_name}"

            response = self.sc_client.search_provisioned_products(
                Filters={"SearchQuery": [f"name:{provisioned_name}"]}
            )

            if response["ProvisionedProducts"]:
                product = response["ProvisionedProducts"][0]
                return ProvisionedProduct(
                    id=product["Id"],
                    name=product["Name"],
                    status=ProvisionedProductStatus(product["Status"]),
                )

            return None

        except self.sc_client.exceptions.ResourceNotFoundException:
            return None

    def get_provisioned_product_by_account_id(
        self, account_id: str
    ) -> Optional[ProvisionedProduct]:
        """
        Get Control Tower provisioned product by account id.

        Args:
            account_id: Id of the AWS account

        Returns:
            ProvisionedProduct if found, None if not found
        """
        try:
            response = self.sc_client.search_provisioned_products(
                Filters={"SearchQuery": [f"physicalId:{account_id}"]}
            )

            if response["ProvisionedProducts"]:
                product = response["ProvisionedProducts"][0]
                return ProvisionedProduct(
                    id=product["Id"],
                    name=product["Name"],
                    status=ProvisionedProductStatus(product["Status"]),
                )

            return None

        except self.sc_client.exceptions.ResourceNotFoundException:
            return None

    def get_provisioned_product_status(
        self, product_id: str
    ) -> Optional[ProvisionedProductStatus]:
        """
        Get current status of a provisioned product.

        Args:
            product_id: ID of the provisioned product

        Returns:
            ProvisionedProductStatus enum if found, None if not found
        """
        try:
            response = self.sc_client.describe_provisioned_product(Id=product_id)

            status = response["ProvisionedProductDetail"]["Status"]
            return ProvisionedProductStatus(status)

        except self.sc_client.exceptions.ResourceNotFoundException:
            return None

    def _get_control_tower_product_id(self) -> str:
        """
        Get the AWS Control Tower Account Factory product ID.

        Raises:
            Exception: When Control Tower Account Factory product is not found
        """
        try:
            response = self.sc_client.search_products_as_admin(
                Filters={"FullTextSearch": ["AWS Control Tower Account Factory"]},
            )

            if not response["ProductViewDetails"]:
                raise Exception(error_msg_control_tower_missing)

            return response["ProductViewDetails"][0]["ProductViewSummary"]["ProductId"]

        except (
            botocore.exceptions.ClientError,
            botocore.exceptions.BotoCoreError,
        ) as e:
            raise Exception(f"Failed to get Control Tower product ID: {str(e)}")

    def _get_artifact_id_if_current(
        self, artifact_and_product_id: tuple[str, str]
    ) -> Optional[str]:
        """
        Retrieves the artifact ID if it is currently active for the given product ID.

        Args:
            - artifact_and_product_id: A tuple containing the artifact ID and product ID.

        Returns:
            The artifact ID if it is currently active, otherwise None.
        """
        product_id, artifact_id = artifact_and_product_id
        response = self.sc_client.describe_provisioning_artifact(
            ProductId=product_id,
            ProvisioningArtifactId=artifact_id,
        )

        return (
            artifact_id
            if response["ProvisioningArtifactDetail"].get("Active", False)
            else None
        )

    def _get_product_artifact_id(self, product_id: str) -> str:
        """
        Get the latest provisioning artifact ID for a product.

        Args:
            product_id: The Service Catalog product ID

        Returns:
            The ID of the latest active provisioning artifact

        Raises:
            Exception: When no active artifacts are found or on AWS API errors
        """
        try:
            response = self.sc_client.describe_product_as_admin(
                Id=product_id,
            )

            if not response.get("ProvisioningArtifactSummaries"):
                raise Exception(error_msg_control_tower_missing)

            artifact_summary = response["ProvisioningArtifactSummaries"]

            try:
                return next(
                    (
                        id
                        for id in map(
                            self._get_artifact_id_if_current,
                            (
                                (product_id, artifact_id.get("Id", ""))
                                for artifact_id in artifact_summary
                            ),
                        )
                        if id
                    )
                )
            except StopIteration:
                raise Exception(error_msg_control_tower_missing)

            # import pprint
            # raise Exception(pprint.pp(response))

            # active_artifacts = [
            #    a
            #    for a in response["ProvisioningArtifactSummaries"]
            #    if a and "Status" in a and a["Status"] == "ACTIVE"
            # ]

            # if not active_artifacts:
            #    raise Exception(error_msg_control_tower_missing)

            # latest_artifact = sorted(
            #    active_artifacts, key=lambda x: x["CreatedTime"], reverse=True
            # )[0]

            # return latest_artifact["Id"]

        except (
            botocore.exceptions.ClientError,
            botocore.exceptions.BotoCoreError,
        ) as e:
            raise Exception(f"Failed to get product artifact ID: {str(e)}")

    def create_control_tower_account(
        self,
        account_name: str,
        account_email: str,
        ou_name: str,
        sso_user_email: str,
        sso_user_first_name: str,
        sso_user_last_name: str,
    ) -> str:
        """
        Create a new account through Control Tower.

        Account creation is an async process, this function only starts that process.

        Args:
            account_name: Name for the new account
            account_email: Email for the new account
            ou_name: Organizational Unit name
            sso_user_email: Email for SSO user
            sso_user_first_name: First name for SSO user
            sso_user_last_name: Last name for SSO user

        Returns:
            The record_id of the provisioned product that will be created
        """
        product_id = self._get_control_tower_product_id()
        artifact_id = self._get_product_artifact_id(product_id)

        provisioning_parameters = [
            {"Key": "AccountEmail", "Value": account_email},
            {"Key": "AccountName", "Value": account_name},
            {"Key": "ManagedOrganizationalUnit", "Value": ou_name},
            {"Key": "SSOUserEmail", "Value": sso_user_email},
            {"Key": "SSOUserFirstName", "Value": sso_user_first_name},
            {"Key": "SSOUserLastName", "Value": sso_user_last_name},
        ]

        response = self.sc_client.provision_product(
            ProductId=product_id,
            ProvisioningArtifactId=artifact_id,
            ProvisionedProductName=account_name,
            ProvisioningParameters=provisioning_parameters,
        )

        record_id = response["RecordDetail"]["RecordId"]
        return record_id

    def terminate_provisioned_product(self, product_id: str) -> str:
        """
        Terminate a provisioned product.

        Args:
            product_id: The ID of the provisioned product to terminate

        Returns:
            str: The record ID of the termination request

        Raises:
            Exception: When termination request fails
        """
        try:
            response = self.sc_client.terminate_provisioned_product(
                aws_retry=True,
                ProvisionedProductId=product_id,
                TerminateToken=str(uuid.uuid4()),  # Unique token for idempotency
                IgnoreErrors=False,
            )

            return response["RecordDetail"]["RecordId"]

        except (
            botocore.exceptions.ClientError,
            botocore.exceptions.BotoCoreError,
        ) as e:
            raise Exception(
                f"Failed to terminate provisioned product {product_id}: {str(e)}"
            )

    def terminate_control_tower_account(self, account_id: str) -> str:
        """
        Terminate a Control Tower managed account.

        Args:
            account_id: The AWS account ID to terminate

        Returns:
            str: The record ID for tracking the termination operation

        Raises:
            Exception: When termination request fails
        """
        try:
            response = self.sc_client.search_provisioned_products(
                aws_retry=True, Filters={"SearchQuery": [f"physicalId:{account_id}"]}
            )

            if not response["ProvisionedProducts"]:
                raise Exception(
                    f"No provisioned product found for account {account_id}"
                )

            provisioned_product = response["ProvisionedProducts"][0]

            terminate_response = self.sc_client.terminate_provisioned_product(
                aws_retry=True,
                ProvisionedProductId=provisioned_product["Id"],
                TerminateToken=str(uuid.uuid4()),  # Unique token for idempotency
                IgnoreErrors=False,
            )

            return terminate_response["RecordDetail"]["RecordId"]

        except (
            botocore.exceptions.ClientError,
            botocore.exceptions.BotoCoreError,
        ) as e:
            raise Exception(f"Failed to terminate account {account_id}: {str(e)}")

    def close_account(self, account_id: str) -> None:
        """
        Close/terminate an AWS account.

        Args:
            account_id: The AWS account ID to close

        Raises:
            Exception: When account closure fails
        """
        try:
            self.org_client.close_account(aws_retry=True, AccountId=account_id)
        except (ClientError, BotoCoreError) as e:
            raise Exception(f"Failed to close account {account_id}: {str(e)}")

    def move_account_to_ou(self, account_id: str, ou_name: str) -> bool:
        """
        Move an AWS account to a specified Organizational Unit (OU) in AWS Organizations.

        Args:
            ou_name (str): Name of the destination OU
            account_id (str): The 12-digit AWS account ID to move

        Returns:
          bool: True if the account was moved successfully, False otherwise

        Raises:
            Exception: If there are errors during the operation
        """
        try:
            if not account_id or not ou_name:
                raise ValueError("Both account_id and ou_name must be provided")

            roots = self.org_client.list_roots()
            if not roots.get("Roots"):
                raise Exception("No roots found in the AWS Organization")
            root_id = roots["Roots"][0]["Id"]

            ou_id = None
            paginator = self.org_client.get_paginator(
                "list_organizational_units_for_parent"
            )
            for page in paginator.paginate(ParentId=root_id):
                for ou in page["OrganizationalUnits"]:
                    if ou["Name"] == ou_name:
                        ou_id = ou["Id"]
                        break
                if ou_id:
                    break

            if not ou_id:
                raise ValueError(f"OU with name {ou_name} not found")

            parents = self.org_client.list_parents(ChildId=account_id)
            if not parents.get("Parents"):
                raise Exception(f"Account {account_id} not found in organization")

            current_parent_id = parents["Parents"][0]["Id"]

            if current_parent_id != ou_id:
                self.org_client.move_account(
                    AccountId=account_id,
                    SourceParentId=current_parent_id,
                    DestinationParentId=ou_id,
                )
                return True
            return False
        except self.org_client.exceptions.AccountNotFoundException:
            raise Exception(f"Error: Account {account_id} not found")
        except self.org_client.exceptions.InvalidInputException as e:
            raise Exception(f"Error: Invalid input - {str(e)}")
        except self.org_client.exceptions.SourceParentNotFoundException:
            raise Exception(f"Error: Source parent not found")
        except self.org_client.exceptions.DestinationParentNotFoundException:
            raise Exception(f"Error: Destination OU not found")
        except self.org_client.exceptions.DuplicateAccountException:
            raise Exception(f"Error: Account {account_id} is already being moved")
        except Exception as e:
            raise Exception(f"Error moving account: {str(e)}")
